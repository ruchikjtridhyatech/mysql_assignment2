# 1.The members details arranged in decreasing order of Date of Birth
select * from members order by date_of_birth desc;


# 2.The members details sorting using two columns(gender,date_of_birth); 
# the first one is sorted in ascending order by default 
# while the second column is sorted in descending order
select gender,date_of_birth from members order by gender, date_of_birth desc;


# 3.The members details get the unique values for genders
select distinct gender from members;

# 4.The movies detials get a list of movie category_id  and 
# corresponding years in which they were released 
select category_id, year_released from movies;

 # 5.The members table - get total number of males and females
select gender,count(gender) from members group by gender;

# 6.The movies table - get all the movies that have the word 
# "code" as part of the title, we would use the percentage 
# wildcard to perform a pattern match on both sides of the word "code".
select title from movies where title like '%Code%';

# 7.The movies table - get all the movies that were released in the year "200X" 
# (using _ underscore wildcard)
select title, year_released from movies where year_released like '200_';


# 8.The movies table - get movies that were not released in the year 200x
select title, year_released from movies where year_released not like '200_';


# 9.The movies table  - movie titles in upper case letters
select upper(title) from movies;


# 10.Create new table "movierentals" (see movierentals.png image)
create table movierentals 
	(reference_number int primary key AUTO_INCREMENT, 
	transaction_date date, 
	return_date date, 
	membership_number int, 
	movie_id int, 
	movie_returned boolean);


# 11.The movierentals table - get the number of times that the movie with id 2 has been rented out
select count(movie_id) from movierentals where movie_id = 2;


# 12.The movierentals table - omits duplicate records which have same movie_id
select * from movierentals group by movie_id;


# 13.The movie table - latest movie year released(using MAX function)
select * from movies where year_released = (select MAX(year_released) from movies);


#14.Create "payments" table (see payments.png image)
create table payments 
	(payment_id int primary key AUTO_INCREMENT,
	membership_number int, 
	payment_date date, 
	description varchar(100),
	amount_paid int,
	external_reference_number int); 



# 15.The payments table - find the average amount paid
select AVG(amount_paid) from payments;


# 16.Add a new field to the members table
ALTER TABLE members ADD COLUMN new_column varchar(100) AFTER email;


# 17.Delete a database from MySQL server (DROP command is used )
DROP DATABASE dummy_database;


# 18.Delete an object (like Table , Column)from a database.(DROP command is used )
ALTER TABLE members DROP new_column;


# 19.DROP a table from Database
DROP TABLE dummy_table;


# 20.Rename table `movierentals` TO `movie_rentals`;
RENAME TABLE movierentals to movie_rentals;


# 21.The member table  - changes the width of "fullname" field from 250 to 50
ALTER TABLE members CHANGE full_names full_names VARCHAR(50);


#22.The member table  - Getting a list of ten (10) members only from the database
select * from members limit 10;


# 23.The member table - gets data starting the second row and limits the results to 2
select * from members limit 1,2;